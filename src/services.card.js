import React from "react";
import SingleServices from "./singleService";

export default function ServicesCard(props) {
return(
    <div>
        {props.services?.map((service) => {
            return <SingleServices service={service}/>
        })}
        
    </div>
)
}