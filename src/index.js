import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route, Router, Navigate } from 'react-router-dom';
import LabelRef1 from './label.ref1';
import LabelRef2 from './label.ref2';
import LabelRef3 from './label.ref3';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
           <Route path="/catalog" element={<App />}>
        </Route>
        <Route path="/" element={<Navigate to="/catalog" replace />} />
        <Route path="/labelling1/:vp_did" element={<LabelRef1 />}>
        </Route>
        <Route path="/labelling2/:vp_did" element={<LabelRef2 />}>
        </Route>
        <Route path="/labelling3/:vp_did" element={<LabelRef3 />}>
        </Route>
      </Routes>
   </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
