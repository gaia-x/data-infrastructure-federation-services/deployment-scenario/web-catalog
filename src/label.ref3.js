import { Button } from 'react-bootstrap'
import './label.ref.css'
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import { faBell } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import axios from 'axios'


function LabelRef3() {

    const [vp, setVP] = useState()
    const [security, setSecurity] = useState(false)
    const [contractual, setContractual] = useState(false)
    const [transparency, setTransparency] = useState(false)
    const [dataProtection, setDataProtection] = useState(false)
    const [portability, setPortability] = useState(false)
    const [europeanControl, setEuropeanControl] = useState(false)

    const [error, setError] = useState(false)
    const { vp_did } = useParams();

    console.log(vp_did)

    function checkVP() {

        if(vp_did === undefined) {
            setError(true)
        }

        const did = vp_did
        const did_vp = did.replaceAll(":", "/")
        const urlVP = did_vp.replace("did/web/", "https://")
        console.log(urlVP)

        axios.get(urlVP).then(resp => {

            console.log(resp.data);

            axios({
                method: 'POST',
                url: 'https://policyrules.abc-federation.gaia-x.community/v1/data/rules/labelling/label/lstValidCriterionForLabel3',
                data: JSON.stringify({ input: resp.data })
            }).then(response => {

                let obj = JSON.stringify(response.data);
                console.log(obj)

                let respObj = JSON.parse(obj).result[0]
                console.log(respObj)

                if (respObj === "crit32to51") {
                    setSecurity(true)
                }
            })
        });
    }

    useEffect(() => {
        checkVP();
    }, []);


    console.log(security)

    return (
        <div>

            <Navbar expand="lg" className='catalog-nav'>

                <Container className='container-nav'>
                    <div>
                        <Navbar.Brand href="#home"><svg width="23" height="23" viewBox="0 0 23 23" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.76562 9.6875C6.02083 9.6875 5.27604 9.41667 4.73438
    8.80729L0.9375 5.00521C-0.213542 3.85417 -0.213542 2.02083
    0.9375 0.937499C2.08854 -0.21875 3.92187 -0.21875 5.00521
    0.937499L8.79688 4.73437C9.95313 5.89062 9.95313 7.71875 8.79688
    8.80729C8.25521 9.41667 7.51042 9.6875 6.76562 9.6875Z"
                                fill="#000F8E" />
                            <path d="M16.0521 9.6875C15.3073 9.6875 14.5625 9.41667 14.0156
        8.80729C12.8646 7.65104 12.8646 5.82292 14.0156
        4.73437L17.8125 0.937499C18.9635 -0.21875 20.7969 -0.21875
        21.8802 0.937499C23.0313 2.08854 23.0313 3.92187 21.8802
        5.00521L18.0833 8.80729C17.5417 9.41667 16.7969 9.6875
        16.0521 9.6875Z" fill="#000F8E" />
                            <path d="M2.96875 22.8542C2.22396 22.8542 1.47917 22.5834
            0.9375 21.9688C-0.213542 20.8177 -0.213542 18.9844
            0.9375 17.9011L4.73438 14.099C5.88542 12.948 7.71354
            12.948 8.79688 14.099C9.95313 15.2552 9.95313 17.0834
            8.79688 18.1719L5.00521 21.9688C4.39583 22.5834 3.71354
            22.8542 2.96875 22.8542Z" fill="#000F8E" />
                            <path d="M19.8438 22.8542C19.099 22.8542 18.3542 22.5834
                17.8125 21.9688L14.0156 18.1719C12.8646 17.0157
                12.8646 15.1875 14.0156 14.099C15.1719 12.948 17
                12.948 18.0833 14.099L21.8802 17.9011C23.0313
                19.0521 23.0313 20.8855 21.8802 21.9688C21.3385
                22.5834 20.5938 22.8542 19.8438 22.8542Z"
                                fill="#000F8E" />
                        </svg>
                        </Navbar.Brand>
                    </div>
                    <div className='btn-menu'>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <NavDropdown title="Simulate label" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">
                                        Another action
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">
                                        Separated link
                                    </NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link href="#action2">My Company Services |</Nav.Link>
                                <Nav.Link href="#action2"><FontAwesomeIcon className='bell' icon={faBell} /> |</Nav.Link>
                                <div className="pull-left section-profil">
                                    <div className="profil">
                                        <p className="name-profil">AB</p>
                                    </div>
                                    <div className="info-profil">
                                        <p className="company-name">Alice Borda</p>
                                        <p className="company-name">Montblanc IOT</p>
                                    </div>
                                </div>

                            </Nav>
                        </Navbar.Collapse>

                    </div>
                </Container>
            </Navbar>

            <Navbar className="company-search" expand="lg">
                <svg className='starss' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg>
                <Container fluid className="bag-space">
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="me-auto my-2 my-lg-0"
                            style={{ maxHeight: '100px' }}
                            navbarScroll
                        >
                            <NavDropdown className="abc-federation" title="Simulation of Gaia X label level" id="navbarScrollingDropdown">
                                <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                                <NavDropdown.Item href="#action4">
                                    Another action
                                </NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="#action5">
                                    Something else here
                                </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Button href='https://service-description-wizard-tool.dufourstorage.gaia-x.community/' className='lab'><svg className='arrow' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" /></svg></Button>
            <div className='container-btn-label'>
               
                <div className='container-btn'><Button className='btn-lab' href='/labelling1'>Level 1</Button></div>
                <div className='container-btn'><Button className='btn-lab' href='/labelling2'>Level 2</Button></div>
                <div className='container-btn'><Button className='btn-lab' href='/labelling3'>Level 3</Button></div>
            </div>
            <div className="table-wrapper-scroll-y my-custom-scrollbar">
            <table className="table table-bordered table-striped mb-0">
                <thead>
                    <tr className='blue'>
                        <th className='labelling' scope="col"><svg className='start' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className='start' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className='start' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg> Level 3</th>
                        <th scope="row"></th>
                        <th scope="row"></th>
                        <th scope="row"></th>
                        <th className='labelling' scope="col white">100%</th>
                    </tr>
                </thead>
                <tbody>
                        <tr className='criterion'>
                            <th className='bold' scope="row">Contractual governance</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row">0/4</th>
                        </tr>
                    
                            <tr>
                                <th className='align' scope="row">Criterion 1: The provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th className='align' scope="row">Criterion 2: The provider shall have an option for each legally binding act to be governed by EU/EEA/Member State law</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th className='align' scope="row">Criterion 3: The provider shall clearly identify for which parties the legal act is binding</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th className='align' scope="row">Criterion 4: The provider shall ensure that the legally binding act covers the entire provision of the service offering</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>

                            </tr>
                 

                        <tr className='criterion'>
                            <th className='bold' scope="row">Transparency</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th>0/14</th>
                        </tr>
                            <tr>
                                <th scope="row">Criterion 5: The provider shall ensure there are specific provisions regarding service interruptions and business continuity (e.g., by means of a service level agreement), provider's bankruptcy or any other reason by which the provider may cease to exist in law</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 6: The provider shall ensure there are provisions governing the rights of the parties to use the service and any data therein</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 7: The provider shall ensure there are provisions governing changes, regardless of their kind</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 8: The provider shall ensure there are provisions governing aspects regarding copyright or any other intellectual property rights</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 9: The provider shall declare the general location of physicals Resources at urban area level</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 10: The provider shall explain how information about subcontractors and related data localisation will be communicated</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 11: The provider shall communicate to the customer where the applicable jurisdiction(s) of subcontractors will be</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 12: The provider shall include in the contract the contact details where customer may address any queries regarding the service offering and the contract</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 13: The provider shall adopt the Gaia-X trust framework, by which customers may verify provid- er’s compliance</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 14: service offering shall include a policy using a common Domain-Specific Language (DSL) to de- scribe permissions, requirements, and constraints</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 15: service offering requires being operated by service offering provider with a verified identity</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 16: service offering must provide a conformant self-description</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 17: self-description attributes need to be consistent across linked self-descriptions</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 18: service offering consumer needs to have a verified identity provided by the federator</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                
                        <tr className='criterion'>
                            <th className='bold' scope="row">Data Protection</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th>0/13</th>
                        </tr>
                   
                            <tr>
                                <th scope="row">Criterion 19: The provider shall offer the ability to establish a contract under Union or EU/EEA/member state law and specifically addressing GDPR requirements</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 20: The provider shall define the roles and responsibilities of each party</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 21: The provider shall clearly define the technical and organizational measures in accordance with the roles and responsibilities of the parties, including an adequate level of detail</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 22: The provider shall be ultimately bound to instructions of the customer</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 23: The provider shall clearly define how customer may instruct, including by electronic means such as configuration tools or APIs</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 24: The provider shall clearly define if and to which extent third country transfer will take place</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 25: The provider shall clearly define if and to the extent third country transfers will take place, and by which means of Chapter V GDPR these transfers will be protected</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 26: The provider shall clearly define if and to which extent sub-processors will be involved</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 27: The provider shall clearly define if and to the extent sub-processors will be involved, and the measures that are in place regarding sub-processors management</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 28: The provider shall define the audit rights for the Customer</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 29: In case of a joint controllership, the provider shall ensure an arrangement pursuant to Art. 26 (1) GDPR is in place</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 30: In case of a joint controllership, at a minimum, the provider shall ensure that the very essence of such agreement is communicated to data subjects</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 31: In case of a joint controllership, the provider shall publish a point of contact for data subjects</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                  
                        <tr className='criterion'>
                            <th className='bold' scope="row">Security</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th>20/20</th>
                        </tr>
                    
                            <tr>
                                <th scope="row">Criterion 32: Organisation of information security: plan, implement, maintain, and continuously improve the information security framework within the organisation</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 33: Information Security Policies: Provide a global information security policy, derived into policies and procedures regarding security requirements and to support business requirements</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 34: risk management: Ensure that risks related to information security are properly identified, assessed, and treated, and that the residual risk is acceptable to the CSP</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 35: Human Resources: Ensure that employees understand their responsibilities, are aware of their responsibilities regarding information security, and that the organisation's assets are protected in the event of changes in responsibilities or termination</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 36: Asset Management: Identify the organisation's own assets and ensure an appropriate level of protection throughout their lifecycle</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 37: Physical Security: Prevent unauthorised physical access and protect against theft, damage, loss, and outage of operations</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 38: Operational Security: Ensure proper and regular operation, including appropriate measures for planning and monitoring capacity, protection against malware, logging, and monitoring events, and dealing with vulnerabilities, malfunctions, and failures</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 39: identity, authentication, and access control management: limit access to information and information processing facilities</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 40: cryptography and key management: ensure appropriate and effective use of cryptography to protect the confidentiality, authenticity, or integrity of information</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 41: communication security: Ensure the protection of information in networks and the corresponding information processing systems</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 42: portability and interoperability: enable the ability to access the cloud service via other cloud services or IT systems of the cloud customers, to obtain the stored data at the end of the contractual relationship and to securely delete it from the cloud service provider</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 43: change and configuration management: ensure that changes and configuration actions to information systems guarantee the security of the delivered cloud service</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 44: development of information systems: ensure information security in the development cycle of information systems</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 45: Procurement Management: Ensure the protection of information that suppliers of the CSP can access and monitor the agreed services and security requirements</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 46: incident management: Ensure a consistent and comprehensive approach to the capture, assessment, communication, and escalation of security incidents</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 47: business continuity: plan, implement, maintain, and test procedures and measures for business continuity and emergency management</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 48: compliance: avoid non-compliance with legal, regulatory, self-imposed, or contractual information security and compliance requirements</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 49: user documentation: provide up-to-date information on the secure configuration and known vulnerabilities of the cloud service for cloud customers</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 50: dealing with information requests from government agencies: Ensure appropriate handling of government investigation requests for legal review, information to cloud customers, and limitation of access to or disclosure of data</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 51: product safety and security: provide appropriate mechanisms for cloud customers</th>
                                <td><svg className={security ? "round green" : "round red"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>ISO27001 / SecNumCloud</td>
                                <td>VP.json</td>
                                <td><svg className={security ? "checking" : "checkbad"} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                 
                        <tr className='criterion'>
                            <th className='bold' scope="row">Portability</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th>0/2</th>
                        </tr>
                
                            <tr>
                                <th scope="row">Criterion 52: The provider shall implement practices for facilitating the switching of providers and the porting of data in a structured, commonly used, and machine-readable format including open standard formats where required or requested by the provider receiving the data</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 53: The provider shall ensure pre-contractual information exists, with sufficiently detailed, clear, and transparent information regarding the processes of data portability, technical requirements, timeframes, and charges that apply in case a professional user wants to switch to another provider or port data back to its own IT systems</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                  
                        <tr className='criterion'>
                            <th className='bold' scope="row">European Control</th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th scope="row"></th>
                            <th>0/8</th>
                        </tr>
                  
                            <tr>
                                <th scope="row">Criterion 54: For label level 2, the provider shall provide the option that all data are processed and stored exclusively in EU/EEA</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 55: For label level 3, the provider shall process and store all data exclusively in the EU/EEA</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 56: For label level 3, where the provider or subcontractor is subject to legal obligations to transmit or disclose data based on a non-EU/EEA statutory order, the provider shall have verified safeguards in place to ensure that any access request is compliant with EU/EEA/Member State law</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 57: For label level 3, the provider’s registered head office, headquarters and main establishment shall be established in a member state of the EU/EEA</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 58: For label level 3, shareholders in the provider, whose registered head office, headquarters, and main establishment are not established in a member state of the EU shall not, directly, or indirectly, individually, or jointly, hold control of the CSP. Control is defined as the ability of a natural or legal person to exercise decisive influence directly or indirectly on the CSP through one or more intermediate entities, de jure or de facto. (cf. Council Regulation No 139/2004 and Commission Consolidated Jurisdictional Notice under Council Regulation (EC) No 139/2004 for illustrations of decisive control)</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 59: For label level 3, in the event of recourse by the provider, in the context of the services provided to the customer, to the services of a third-party company - including a subcontractor - whose registered head office, headquarters and main establishment is outside of the European Union or who is owned or controlled directly or indirectly by another third-party company registered outside the EU/EEA, the third-party company shall have no access over the customer data nor access and identity management for the services provided to the customer. The provider, including any of its sub-processor, shall push back any request received from non-european authorities to obtain communication of personal data relating to european customers, except if request is made in execution of a court judgment or order that is valid and compliant under Union law and applicable member states law as provided by Article 48 GDPR</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 60: For label level 3, the provider must guarantee continuous autonomy for all or part of the services it provides. The concept of operating autonomy shall be understood as the ability to maintain the provision of the cloud computing service by drawing on the provider’s own skills or by using adequate alternatives</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                            <tr>
                                <th scope="row">Criterion 61: The provider shall not access customer data unless authorised by the customer or when the access is in accordance with EU/EEA/member state law</th>
                                <td><svg className='round red' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512z" /></svg></td>
                                <td>Reference</td>
                                <td>Document-name.pdf</td>
                                <td><svg className='checkbad' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg></td>
                            </tr>
                    
                    </tbody>
                    
                </table>
                </div>
                </div>
    )
}

export default LabelRef3