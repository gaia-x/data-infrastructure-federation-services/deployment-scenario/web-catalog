import './App.css';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import axios from 'axios'
import ServicesCard from './services.card';
import SearchBar from './search';
import filtersJson from './filters.json'
import Alert from 'react-bootstrap/Alert';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { faBell } from '@fortawesome/free-regular-svg-icons'
//import { NavLink } from 'react-bootstrap';

function App() {
  const [services, setServices] = useState();
  const [servicesCopy, setServicesCopy] = useState();
  const [filters, setFilters] = useState(false)
  const [data, setData] = useState(services)
  const [loading, setLoading] = useState(true)

  const [checkProvider, setCheckProvider] = useState("")
  const [checkLayer, setCheckLayer] = useState("")
  const [checkLocation, setCheckLocation] = useState("")
  const [checkType, setCheckType] = useState("")
  const [checkCertif, setCheckCertif] = useState("")
  const [checkLabel, setCheckLabel] = useState("")

  const [type, setType] = useState("")
  const [layer, setLayer] = useState("")
  const [provider, setProvider] = useState("")
  const [location, setLocation] = useState("")
  const [certif, setCertif] = useState("")
  const [label, setLabel] = useState("")

  const [typeFilter, setTypeFilter] = useState()
  const [layerFilter, setLayerFilter] = useState()
  const [providerFilter, setProviderFilter] = useState()
  
  const [security, setSecurity] = useState()
  const [dataProtection, setDataProtection] = useState()
  const [climateNeutrality, setClimateNeutrality] = useState()
  const [dataPorting, setDataPorting] = useState()
  const [other, setOther] = useState()

  const [count, setCount] = useState()
  const [countCopy, setCountCopy] = useState()
  const [checkCount, setCheckCount] = useState(false)

  const [show, setShow] = useState(false);
  const [showw, setShoww] = useState(true);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  async function getService() {
    axios({
      method: 'post',
      url: 'https://federated-catalogue-api.abc-federation.gaia-x.community/query',
      data: {}
    })
      .then(function (response) {
        if (response.data) {
          setServices(response.data)
          setCount(response.data.length)
          setLoading(false)
        } else {
          return [];
        }
      })

      axios({
        method: 'get',
        url: 'https://federated-catalogue-api.abc-federation.gaia-x.community/api/service_types',
        data: {}
      })
        .then(function (response) {
          if (response.data) {
            setTypeFilter(response.data)
          } else {
            return [];
          }
        })

        axios({
          method: 'get',
          url: 'https://federated-catalogue-api.abc-federation.gaia-x.community/api/layers',
          data: {}
        })
          .then(function (response) {
            if (response.data) {
              setLayerFilter(response.data)
            } else {
              return [];
            }
          })

          axios({
            method: 'get',
            url: 'https://federated-catalogue-api.abc-federation.gaia-x.community/api/providers',
            data: {}
          })
            .then(function (response) {
              if (response.data) {
                setProviderFilter(response.data)
              } else {
                return [];
              }
            })
            
            axios({
              method: 'get',
              url: 'https://federated-catalogue-api.abc-federation.gaia-x.community/api/compliance_references',
              data: {}
            })
              .then(function (response) {
                if (response.data) {
                  let secur = []
                  let dataPro = []
                  let climateNeutra = []
                  let dataPort = []
                  let oth = []
                  for(let i = 0; i < response.data.length; i++) {
                    if(response.data[i].type === "Security") {
                      secur.push(response.data[i].title)
                      setSecurity(secur)
                    } else if(response.data[i].type === "Data Protection") {
                      dataPro.push(response.data[i].title)
                      setDataProtection(dataPro)
                    } else if(response.data[i].type === "Climate Neutrality") {
                      climateNeutra.push(response.data[i].title)
                      setClimateNeutrality(climateNeutra)
                    } else if(response.data[i].type === "Data Porting") {
                      dataPort.push(response.data[i].title)
                      setDataPorting(dataPort)
                    } else if(response.data[i].type === "Other") {
                      oth.push(response.data[i].title)
                      setOther(oth)
                    }
                  }
                } else {
                  return [];
                }
              })

  }

  useEffect(() => {
      getService();
  }, []);


  function selection(e, className) {
    const element = document.querySelector("." + className)

    if (element) {
      element.classList.remove(className)
      e.currentTarget.classList.toggle(className)
    } else {
      e.currentTarget.classList.toggle(className)
    }

  }

  async function handleLabel(e) {

    let response = parseInt(e.target.value)

    setLabel(response)
    setCheckLabel("label")

    if (e.target.value === "") {
      setLabel("")
      response = ""
      setCheckLabel("")
    }

    selection(e, "selec-label")

  }

  async function handleCertif(e) {
    let response = e.target.value

    setCertif(response)
    setCheckCertif("certif")

    if (e.target.value === "") {
      response = ""
      setCheckCertif("")
    }

  }

  async function handleLocation(e) {
    let response = e.target.value

    setLocation(response)
    setCheckLocation("location")

    if (e.target.value === "") {
      response = ""
      setCheckLocation("")
    }

  }

  async function handleProvider(e) {
    let response = e.target.value

    setProvider(response)
    setCheckProvider("provider")

    if (e.target.value === "") {
      response = ""
      setCheckProvider("")
    }

    selection(e, "selec-provider")

  }

  async function handleLayer(e) {
    let response = e.target.value

    setLayer(response)
    setCheckLayer("layer")

    if (e.target.value === "") {
      response = ""
      setCheckLayer("")
    }

    selection(e, "selec-layer")
  }

  async function handleTypes(e) {
    let response = e.target.value

    setType(response)
    setCheckType("type")

    if (e.target.value === "") {
      response = ""
      setCheckType("")
    }

    selection(e, "selec-type")

  }

  async function addFilter(e) {
    handleProvider(e)
    handleLayer(e)
    handleLocation(e)
    handleTypes(e)
    handleCertif(e)
    handleLabel(e)

    let tab = [type, provider, layer, location, label, certif]
    let fields = [checkType, checkProvider, checkLayer, checkLocation, checkLabel, checkCertif]

    let fieldsTrue = []
    let fieldsFilter = []
    let dataFilter = []

    for (let i = 0; i < tab.length; i++) {
      if (tab[i] === '') {

      } else {
        dataFilter.push(tab[i])
        fieldsTrue.push(fields[i])
        fieldsFilter.push(filtersJson[0][fields[i]])
      }
    }

    let resp = services

    for (let a = 0; a < dataFilter.length; a++) {
      if (dataFilter[a] === "Paris" || dataFilter[a] === "Roubaix" || dataFilter[a] === "Gravelines" || dataFilter[a] === "Strasbourg" || dataFilter[a] === "Frankfurt" || dataFilter[a] === "Frankfurt am Main" || dataFilter[a] === "Milan" || dataFilter[a] === "Arezzo" || dataFilter[a] === "Merate" || dataFilter[a] === "Garbagnate" || dataFilter[a] === "Grenoble" || dataFilter[a] === "Milano" || dataFilter[a] === "London" || dataFilter[a] === "Erith" || dataFilter[a] === "Warsaw" || dataFilter[a] === "Ozarow" || dataFilter[a] === "Madrid" || dataFilter[a] === "Alcobendas" || dataFilter[a] === "Port" || dataFilter[a] === "Matinha" || dataFilter[a] === "Hillsboro" || dataFilter[a] === "Vint Hill" || dataFilter[a] === "Marcoussis" || dataFilter[a] === "Pantin" || dataFilter[a] === "Marseille" || dataFilter[a] === "Aix-En-Provence") {
        fieldsFilter[a] = "urban_area"
        resp = resp.filter(service => service[fieldsFilter[a]].toLowerCase().includes(dataFilter[a].toLocaleLowerCase()))

      } else if (!isNaN(dataFilter[a])) {
        resp = resp.filter(service => service[fieldsFilter[a]] === (dataFilter[a]))

      } else {
        resp = resp.filter(service => service[fieldsFilter[a]].toLowerCase().includes(dataFilter[a].toLocaleLowerCase()))
      }

    }

    let counter = resp.length
    setServicesCopy(resp)
    setFilters(true)

    setCountCopy(counter)
    setCheckCount(true)
    setShow(false)
  }

  function resetFilters(className) {
    const element = document.querySelector("." + className)
    if (element) {
      element.classList.remove(className)
    }
  }

  async function closeHandleBtn() {
    setFilters(false)
    setCheckCount(false)
    resetFilters("selec-provider")
    resetFilters("selec-type")
    resetFilters("selec-layer")
    resetFilters("selec-type")
  }

  return (
    <body>
      <div>
        <header>
          <div>
            <Navbar expand="lg" className='catalog-nav'>

              <Container className='container-nav'>
                <div>
                  <Navbar.Brand href="#"><svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.76562 9.6875C6.02083 9.6875 5.27604 9.41667 4.73438
                8.80729L0.9375 5.00521C-0.213542 3.85417 -0.213542 2.02083
                0.9375 0.937499C2.08854 -0.21875 3.92187 -0.21875 5.00521
                0.937499L8.79688 4.73437C9.95313 5.89062 9.95313 7.71875 8.79688
                8.80729C8.25521 9.41667 7.51042 9.6875 6.76562 9.6875Z"
                      fill="#000F8E" />
                    <path d="M16.0521 9.6875C15.3073 9.6875 14.5625 9.41667 14.0156
                    8.80729C12.8646 7.65104 12.8646 5.82292 14.0156
                    4.73437L17.8125 0.937499C18.9635 -0.21875 20.7969 -0.21875
                    21.8802 0.937499C23.0313 2.08854 23.0313 3.92187 21.8802
                    5.00521L18.0833 8.80729C17.5417 9.41667 16.7969 9.6875
                    16.0521 9.6875Z" fill="#000F8E" />
                    <path d="M2.96875 22.8542C2.22396 22.8542 1.47917 22.5834
                        0.9375 21.9688C-0.213542 20.8177 -0.213542 18.9844
                        0.9375 17.9011L4.73438 14.099C5.88542 12.948 7.71354
                        12.948 8.79688 14.099C9.95313 15.2552 9.95313 17.0834
                        8.79688 18.1719L5.00521 21.9688C4.39583 22.5834 3.71354
                        22.8542 2.96875 22.8542Z" fill="#000F8E" />
                    <path d="M19.8438 22.8542C19.099 22.8542 18.3542 22.5834
                            17.8125 21.9688L14.0156 18.1719C12.8646 17.0157
                            12.8646 15.1875 14.0156 14.099C15.1719 12.948 17
                            12.948 18.0833 14.099L21.8802 17.9011C23.0313
                            19.0521 23.0313 20.8855 21.8802 21.9688C21.3385
                            22.5834 20.5938 22.8542 19.8438 22.8542Z"
                      fill="#000F8E" />
                  </svg>
                  </Navbar.Brand>
                </div>
                <div className='btn-menu'>
                  <Navbar.Toggle aria-controls="basic-navbar-nav" />
                  <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                      <NavDropdown title="Catalogue" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">
                          Another action
                        </NavDropdown.Item>
                        <NavDropdown.Item href="#">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#">
                          Separated link
                        </NavDropdown.Item>
                      </NavDropdown>
                      {/* <Nav.Link href="#">My Company Services |</Nav.Link>
                      <Nav.Link href="#action2"><FontAwesomeIcon className='bell' icon={faBell} /> |</Nav.Link> */}

                      <div className="pull-left section-profil">
                        <Nav.Link className='login' href="#action2">Log in</Nav.Link>

                        {/* <div className="profil">
                          <p className="name-profil">AB</p>
                        </div> */}
                        {/* <div className="info-profil">
                          <p className="company-name">Login</p>
                          <p className="company-name">Montblanc IOT</p>
                        </div> */}
                      </div>

                    </Nav>
                  </Navbar.Collapse>

                </div>
              </Container>
            </Navbar>
          </div>

          <Navbar className="company-search" expand="lg">
            <svg className="bag" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M184 48H328c4.4 0 8 3.6 8 8V96H176V56c0-4.4 3.6-8 8-8zm-56 8V96H64C28.7 96 0 124.7 0 160v96H192 320 512V160c0-35.3-28.7-64-64-64H384V56c0-30.9-25.1-56-56-56H184c-30.9 0-56 25.1-56 56zM512 288H320v32c0 17.7-14.3 32-32 32H224c-17.7 0-32-14.3-32-32V288H0V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V288z" /></svg>
            <Container fluid className="bag-space">
              <Navbar.Toggle aria-controls="navbarScroll" />
              <Navbar.Collapse id="navbarScroll">
                <Nav
                  className="me-auto my-2 my-lg-0"
                  style={{ maxHeight: '100px' }}
                  navbarScroll
                ><div>
                <p className='p-banner'>This site is for DEMONSTRATION purposes and technical evaluation of the latest Gaia-X specification v22.10.</p>
                <NavDropdown className="abc-federation" title="All the Verifiable Credentials and Presentations are SIMULATED and are not binding for any organizations including the Gaia-X association and any of the participant to this site design. (see disclaimer below)" id="navbarScrollingDropdown">
                </NavDropdown>
              </div>
                  {/* <NavDropdown className="abc-federation" title="ABC Federation Catalogue" id="navbarScrollingDropdown">
                  </NavDropdown> */}
                </Nav>
                <SearchBar filterField={(item) => { return item.country_name + " " + item.service_title + " " + item.urban_area + " " + item.state + " " + item.description + " " + item.layer.replace("['", "") }} list={services} setList={setData} />
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </header>

        <Alert className='popup' show={showw} variant="success">
          <Alert.Heading>Disclaimer</Alert.Heading>
          <p>
            This site is for demonstration purposes and technical evaluation of Gaia-X specifications for the catalog implementation.
            It aims to implement GAIA-X Trust Framework v22.11 on a service catalog.
            All the Verifiable Credentials and Presentations in the backend are simulated and are not binding for any organizations including GAIA-X AISBL and any of the participants to this site design.
            Therefore content on this site is provided without any warranty and can be removed without any notice at any time.
            By using this site the visitor aknowledges that he understands and agrees with the herebefore statements.
            If you have any questions about this site, please follow this link <a className='contact' href='https://gaia-x.eu/contact/'>https://gaia-x.eu/contact/</a> or <a className='contact' href='https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue'>https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue</a>
          </p>
          <div className="d-flex justify-content-end">
            <Button className='btn-popup' onClick={() => setShoww(false)} variant="outline-success">
              Close
            </Button>
          </div>
        </Alert>

        <div className='section-filters'>
          <div className='show-choice'>
            <p className='p-results'>{checkCount ? countCopy : count && data ? data.length : count} results | </p>
            <svg className='download none' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 32c0-17.7-14.3-32-32-32s-32 14.3-32 32V274.7l-73.4-73.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0l128-128c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L288 274.7V32zM64 352c-35.3 0-64 28.7-64 64v32c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V416c0-35.3-28.7-64-64-64H346.5l-45.3 45.3c-25 25-65.5 25-90.5 0L165.5 352H64zM432 456c-13.3 0-24-10.7-24-24s10.7-24 24-24s24 10.7 24 24s-10.7 24-24 24z" /></svg>
            <Nav.Link href="#" className='link-result none'>Download the results of all services</Nav.Link>
          </div>
          <div className='filter-show'>
            <div className='container-btn-filter none'>
              <Button value='Italy' href='#' className="me-2 btn-filter keyword">
                <svg className='book-logo' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M96 0C43 0 0 43 0 96V416c0 53 43 96 96 96H384h32c17.7 0 32-14.3 32-32s-14.3-32-32-32V384c17.7 0 32-14.3 32-32V32c0-17.7-14.3-32-32-32H384 96zm0 384H352v64H96c-17.7 0-32-14.3-32-32s14.3-32 32-32zm32-240c0-8.8 7.2-16 16-16H336c8.8 0 16 7.2 16 16s-7.2 16-16 16H144c-8.8 0-16-7.2-16-16zm16 48H336c8.8 0 16 7.2 16 16s-7.2 16-16 16H144c-8.8 0-16-7.2-16-16s7.2-16 16-16z" /></svg>Keyword</Button>
            </div>
            <div>
              <div className='container-btn-filter'>
                <Button onClick={handleShow} className="me-2 btn-filter"><svg className='filter-logo' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M3.9 54.9C10.5 40.9 24.5 32 40 32H472c15.5 0 29.5 8.9 36.1 22.9s4.6 30.5-5.2 42.5L320 320.9V448c0 12.1-6.8 23.2-17.7 28.6s-23.8 4.3-33.5-3l-64-48c-8.1-6-12.8-15.5-12.8-25.6V320.9L9 97.3C-.7 85.4-2.8 68.8 3.9 54.9z" /></svg>
                  Filters
                </Button>
              </div>
              <Offcanvas show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>

                  <Offcanvas.Title>Filters</Offcanvas.Title>
                  <Button className='reset-filter' onClick={closeHandleBtn}>reset all</Button>
                </Offcanvas.Header>
                <Offcanvas.Body>
                  <div className='section-filter'>
                    <p><svg className='star' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg>Gaia X label</p>
                    <div>
                      <Button className='btn-label' value="3" onClick={handleLabel}><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg>Level 3</Button>
                      <Button className='btn-label' value="2" onClick={handleLabel}><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg>Level 2</Button>
                      <Button className='btn-label' value="1" onClick={handleLabel}><svg className='stars-btn' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M316.9 18C311.6 7 300.4 0 288.1 0s-23.4 7-28.8 18L195 150.3 51.4 171.5c-12 1.8-22 10.2-25.7 21.7s-.7 24.2 7.9 32.7L137.8 329 113.2 474.7c-2 12 3 24.2 12.9 31.3s23 8 33.8 2.3l128.3-68.5 128.3 68.5c10.8 5.7 23.9 4.9 33.8-2.3s14.9-19.3 12.9-31.3L438.5 329 542.7 225.9c8.6-8.5 11.7-21.2 7.9-32.7s-13.7-19.9-25.7-21.7L381.2 150.3 316.9 18z" /></svg>Level 1</Button>
                      <Button className='btn-label' value="0" onClick={handleLabel}>No label</Button>
                    </div>
                  </div>
                  <div className='section-filter'>
                    <p><svg className='check' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512c141.4 0 256-114.6 256-256S397.4 0 256 0S0 114.6 0 256S114.6 512 256 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z" /></svg>Certification</p>
                    <div>

                      <Form.Select className='select-btn' aria-label="Security" onChange={handleCertif}>
                        <option className='selected' selected >Security</option>
                        {loading ? 
                       ""  : 
                        security.map((secu) => (
                          <option value={secu}>{secu}</option>
                        ))
                      }
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Data Protection" onChange={handleCertif}>
                        <option className='selected' selected >Data Protection</option>
                        {loading ? 
                       ""  : 
                        dataProtection.map((data) => (
                          <option value={data}>{data}</option>
                        ))
                      }
                      </Form.Select>
                      <Form.Select className='select-btn' aria-label="Climate Neutrality" onChange={handleCertif}>
                        <option className='selected' selected >Climate Neutrality</option>
                        {loading ? 
                       ""  : 
                        climateNeutrality.map((climate) => (
                          <option value={climate}>{climate}</option>
                        ))
                      }
                      </Form.Select>
                      <Form.Select className='select-btn' aria-label="Data Porting" onChange={handleCertif}>
                        <option className='selected' selected >Data Porting</option>
                        {loading ? 
                       ""  : 
                        dataPorting.map((data) => (
                          <option value={data}>{data}</option>
                        ))
                      }
                      </Form.Select>
                      <Form.Select className='select-btn' aria-label="Other" onChange={handleCertif}>
                        <option className='selected' selected >Other</option>
                        {loading ? 
                       ""  : 
                        other.map((oth) => (
                          <option value={oth}>{oth}</option>
                        ))
                      }
                      </Form.Select>
                    </div>
                  </div>

                  <div className='section-filter'>
                    <p><svg className='gear mr' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M495.9 166.6c3.2 8.7 .5 18.4-6.4 24.6l-43.3 39.4c1.1 8.3 1.7 16.8 1.7 25.4s-.6 17.1-1.7 25.4l43.3 39.4c6.9 6.2 9.6 15.9 6.4 24.6c-4.4 11.9-9.7 23.3-15.8 34.3l-4.7 8.1c-6.6 11-14 21.4-22.1 31.2c-5.9 7.2-15.7 9.6-24.5 6.8l-55.7-17.7c-13.4 10.3-28.2 18.9-44 25.4l-12.5 57.1c-2 9.1-9 16.3-18.2 17.8c-13.8 2.3-28 3.5-42.5 3.5s-28.7-1.2-42.5-3.5c-9.2-1.5-16.2-8.7-18.2-17.8l-12.5-57.1c-15.8-6.5-30.6-15.1-44-25.4L83.1 425.9c-8.8 2.8-18.6 .3-24.5-6.8c-8.1-9.8-15.5-20.2-22.1-31.2l-4.7-8.1c-6.1-11-11.4-22.4-15.8-34.3c-3.2-8.7-.5-18.4 6.4-24.6l43.3-39.4C64.6 273.1 64 264.6 64 256s.6-17.1 1.7-25.4L22.4 191.2c-6.9-6.2-9.6-15.9-6.4-24.6c4.4-11.9 9.7-23.3 15.8-34.3l4.7-8.1c6.6-11 14-21.4 22.1-31.2c5.9-7.2 15.7-9.6 24.5-6.8l55.7 17.7c13.4-10.3 28.2-18.9 44-25.4l12.5-57.1c2-9.1 9-16.3 18.2-17.8C227.3 1.2 241.5 0 256 0s28.7 1.2 42.5 3.5c9.2 1.5 16.2 8.7 18.2 17.8l12.5 57.1c15.8 6.5 30.6 15.1 44 25.4l55.7-17.7c8.8-2.8 18.6-.3 24.5 6.8c8.1 9.8 15.5 20.2 22.1 31.2l4.7 8.1c6.1 11 11.4 22.4 15.8 34.3zM256 336c44.2 0 80-35.8 80-80s-35.8-80-80-80s-80 35.8-80 80s35.8 80 80 80z" /></svg>Type of service</p>
              
                      {loading ? (
                       <div></div> ) : (<div>
                        {typeFilter.map((type) => (
                          <Button value={type} className='btn-type-service btn-type' onClick={handleTypes}>{type}</Button>
                        ))}
                      </div>)
                      }

                  </div>

                  <div className='section-filter'>
                    <p><svg className='cube mr' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M234.5 5.7c13.9-5 29.1-5 43.1 0l192 68.6C495 83.4 512 107.5 512 134.6V377.4c0 27-17 51.2-42.5 60.3l-192 68.6c-13.9 5-29.1 5-43.1 0l-192-68.6C17 428.6 0 404.5 0 377.4V134.6c0-27 17-51.2 42.5-60.3l192-68.6zM256 66L82.3 128 256 190l173.7-62L256 66zm32 368.6l160-57.1v-188L288 246.6v188z" /></svg>Layer</p>

                    {loading ? (
                       <div></div> ) : (<div>
                        {layerFilter.map((layer) => (
                          <Button value={layer} className='btn-type-service btn-layer' onClick={handleLayer}>{layer}</Button>
                        ))}
                      </div>)
                      }
                  </div>

                  <div className='section-filter'>
                    <p><svg className='globe mr' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M352 256c0 22.2-1.2 43.6-3.3 64H163.3c-2.2-20.4-3.3-41.8-3.3-64s1.2-43.6 3.3-64H348.7c2.2 20.4 3.3 41.8 3.3 64zm28.8-64H503.9c5.3 20.5 8.1 41.9 8.1 64s-2.8 43.5-8.1 64H380.8c2.1-20.6 3.2-42 3.2-64s-1.1-43.4-3.2-64zm112.6-32H376.7c-10-63.9-29.8-117.4-55.3-151.6c78.3 20.7 142 77.5 171.9 151.6zm-149.1 0H167.7c6.1-36.4 15.5-68.6 27-94.7c10.5-23.6 22.2-40.7 33.5-51.5C239.4 3.2 248.7 0 256 0s16.6 3.2 27.8 13.8c11.3 10.8 23 27.9 33.5 51.5c11.6 26 21 58.2 27 94.7zm-209 0H18.6C48.6 85.9 112.2 29.1 190.6 8.4C165.1 42.6 145.3 96.1 135.3 160zM8.1 192H131.2c-2.1 20.6-3.2 42-3.2 64s1.1 43.4 3.2 64H8.1C2.8 299.5 0 278.1 0 256s2.8-43.5 8.1-64zM194.7 446.6c-11.6-26-20.9-58.2-27-94.6H344.3c-6.1 36.4-15.5 68.6-27 94.6c-10.5 23.6-22.2 40.7-33.5 51.5C272.6 508.8 263.3 512 256 512s-16.6-3.2-27.8-13.8c-11.3-10.8-23-27.9-33.5-51.5zM135.3 352c10 63.9 29.8 117.4 55.3 151.6C112.2 482.9 48.6 426.1 18.6 352H135.3zm358.1 0c-30 74.1-93.6 130.9-171.9 151.6c25.5-34.2 45.2-87.7 55.3-151.6H493.4z" /></svg>Location</p>
                    <div>

                      <Form.Select className='select-btn' aria-label="Australia" onChange={handleLocation}>
                        <option className='selected'>Australia</option>
                        <option value="Australia">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Belgium" onChange={handleLocation}>
                        <option className='selected'>Belgium</option>
                        <option value="Belgium">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Canada" onChange={handleLocation}>
                        <option className='selected'>Canada</option>
                        <option value="Canada">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Czech Republic" onChange={handleLocation}>
                        <option className='selected'>Czech Republic</option>
                        <option value="Czech Republic">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="France" onChange={handleLocation}>
                        <option className='selected'>France</option>
                        <option value="France">All cities</option>
                        <option value="Aix-En-Provence">Aix-En-Provence</option>
                        <option value="Gravelines">Gravelines</option>
                        <option value="Grenoble">Grenoble</option>
                        <option value="Marcoussis">Marcoussis</option>
                        <option value="Marseille">Marseille</option>
                        <option value="Pantin">Pantin</option>
                        <option value="Paris">Paris</option>
                        <option value="Roubaix">Roubaix</option>
                        <option value="Strasbourg">Strasbourg</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Germany" onChange={handleLocation}>
                        <option className='selected'>Germany</option>
                        <option value="Germany">All cities</option>
                        <option value="Frankfurt">Frankfurt</option>
                        <option value="Frankfurt am Main">Frankfurt am Main</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Ireland" onChange={handleLocation}>
                        <option className='selected'>Ireland</option>
                        <option value="Ireland">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Italy" onChange={handleLocation}>
                        <option className='selected'>Italy</option>
                        <option value="Italy">All cities</option>
                        <option value="Arezzo">Arezzo</option>
                        <option value="Garbagnate">Garbagnate</option>
                        <option value="Merate">Merate</option>
                        <option value="Milan">Milan</option>
                        <option value="Milano">Milano</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Netherlands" onChange={handleLocation}>
                        <option className='selected'>Netherlands</option>
                        <option value="Netherlands">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Poland" onChange={handleLocation}>
                        <option className='selected'>Poland</option>
                        <option value="Poland">All cities</option>
                        <option value="Ozarow">Ozarow</option>
                        <option value="Warsaw">Warsaw</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Portugal" onChange={handleLocation}>
                        <option className='selected'>Portugal</option>
                        <option value="Portugal">All cities</option>
                        <option value="Matinha">Matinha</option>
                        <option value="Port">Port</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Republic of Singapore" onChange={handleLocation}>
                        <option className='selected'>Republic of Singapore</option>
                        <option value="Republic of Singapore">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Spain" onChange={handleLocation}>
                        <option className='selected'>Spain</option>
                        <option value="Spain">All cities</option>
                        <option value="Alcobendas">Alcobendas</option>
                        <option value="Madrid">Madrid</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="Sweden" onChange={handleLocation}>
                        <option className='selected'>Sweden</option>
                        <option value="Sweden">All cities</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="United Kingdom" onChange={handleLocation}>
                        <option className='selected'>United Kingdom</option>
                        <option value="United Kingdom">All cities</option>
                        <option value="Erith">Erith</option>
                        <option value="London">London</option>
                      </Form.Select>

                      <Form.Select className='select-btn' aria-label="United States" onChange={handleLocation}>
                        <option className='selected'>United States</option>
                        <option value="United States">All cities</option>
                        <option value="Hillsboro">Hillsboro</option>
                        <option value="VintHill">Vint Hill</option>
                      </Form.Select>

                    </div>
                  </div>

                  <div className='section-filter'>
                    <p><svg className='provider mr' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 0c4.6 0 9.2 1 13.4 2.9L457.7 82.8c22 9.3 38.4 31 38.3 57.2c-.5 99.2-41.3 280.7-213.7 363.2c-16.7 8-36.1 8-52.8 0C57.3 420.7 16.5 239.2 16 140c-.1-26.2 16.3-47.9 38.3-57.2L242.7 2.9C246.8 1 251.4 0 256 0zm0 66.8V444.8C394 378 431.1 230.1 432 141.4L256 66.8l0 0z" /></svg>Provider</p>
                    {loading ? (
                       <div></div> ) : (<div>
                        {providerFilter.map((provider) => (
                          <Button value={provider.provider_did} className='btn-type-service btn-provider' onClick={handleProvider}>{provider.designation}</Button>
                        ))}
                      </div>)
                      }
                  </div>

                  <div className='center-btn'>
                    <Button className='btn-submit-filters' closeButton onClick={addFilter}>Apply filters</Button>
                  </div>
                </Offcanvas.Body>
              </Offcanvas>
            </div>
          </div>
        </div>

        {loading ? (
          <div className="loader-container">
            <div className="spinner"></div>
          </div>) : (<ServicesCard services={filters ? servicesCopy : services && data ? data : services} addFilter={addFilter} />)
        }

      </div>

    </body>
  );
}

export default App;