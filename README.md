# Gaia-X web catalog

The gaia-x catalog presents services compliant with the Gaia-X Trust Framework. The catalog is available at this URL: https://dev.webcatalog.gaia-x.community


## Available Scripts

In the project directory, you can run:

* Clone the project locally:
```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog.git
``` 

* Runs the app in the development mode.\
```
npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

* Launches the test runner in the interactive watch mode.\
```
npm test
```

* Builds the app for production to the `build` folder.\
```
npm run build
```


## API used by the catalog

* Return services:
```
curl -X POST -H "Content-Type: application/json" -d “{}” https://federated-catalogue-api.abc-federation.gaia-x.community/query
```

example output :
```
[
    {
        "compliance_label_level": 3,
        "compliance_reference_title": "['SOC type I', 'ISO 27001', 'ISO 27017', 'CISPE Data Protection CoC', 'ISO 9001', 'ISO 27018']",
        "country_name": "Italy",
        "description": "Cloud Backup is a service offered by ARUBA",
        "keyword": "[ 'Backup']",
        "layer": "[ 'SAAS']",
        "located_service_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/located-service-offering/faa8908acadda382a69907984c3c02b7b6d025d38d9fb513ab07e93b2a800b91/data.json",
        "located_service_vc": "['did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/None/0cf27a72be8c13b2347a6493086a1afd407c5f8a9fdc1b6e03750140a28db1cf/data.json', 'did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/0cf27a72be8c13b2347a6493086a1afd407c5f8a9fdc1b6e03750140a28db1cf/data.json', 'did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/41ca94c77c384785b61530e9773cb3855ee24058d8adaf8b305db64f4ce71395/data.json', 'did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/ea6472323445ad415cb3369cbb5914be663baa440b32d808af81ba4edd0d2203/data.json']",
        "location_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/location/f17220a5c93bdc04451b1253015b4e2c2dc1cd532b9e4e6e75f13f39080024f6/data.json",
        "provider_designation": "IT1",
        "provider_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/data.json",
        "provider_service_id": "S-06",
        "service_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/service-offering/13b6b3e29c0c079811feae9761470e0d352d4890f76c790fb160620f98cc3e49/data.json",
        "service_title": "Cloud Backup",
        "service_type": "[ 'Backup']",
        "state": "AR",
        "urban_area": "Arezzo"
    },
    {
        "compliance_label_level": 3,
        "compliance_reference_title": "['SOC type I', 'ISO 27001', 'ISO 27017', 'CISPE Data Protection CoC', 'ISO 9001', 'ISO 27018']",
        "country_name": "Italy",
        "description": "Cloud Backup is a service offered by ARUBA",
        "keyword": "[ 'Backup']",
        "layer": "[ 'SAAS']",
        "located_service_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/located-service-offering/eb12a4aaa3db36ca79a9018d45f5ee2d9bbb9348b0f170d36fc70161e8ed25ff/data.json",
        "located_service_vc": "['did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/1243c2033040d14881488f2b6119e80794cf176cc1e0a702817b961520675618/data.json', 'did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/vc/f674a0c2fd21fdbfe3bebf15723a97439ec859ef603815f200e3810e4ee89835/data.json']",
        "location_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json",
        "provider_designation": "IT3",
        "provider_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/data.json",
        "provider_service_id": "S-06",
        "service_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/service-offering/13b6b3e29c0c079811feae9761470e0d352d4890f76c790fb160620f98cc3e49/data.json",
        "service_title": "Cloud Backup",
        "service_type": "[ 'Backup']",
        "state": "BG",
        "urban_area": "Ponte San Pietro"
    },
    ... 
    ]
```

* Return services types:
```
curl -X GET -H "Content-Type: application/json" https://federated-catalogue-api.abc-federation.gaia-x.community/api/service_types
```

example output:
```
["Authentication", "Backup", "Bare Metal", "Collaboration", "Compute", "Container", "Data Analytics", "Data Base", "Digital Signature", "ID management", "IoT", "Logs", "Machine Learning", "Networking", "Storage", "Translation", "Virtual Machine", "VPN", "VPS", "Webhosting"]
```

* Return poviders:
```
curl -X GET -H "Content-Type: application/json" https://federated-catalogue-api.abc-federation.gaia-x.community/api/providers
```

example output:
```
[
  {
    "designation": "ARUBA S.P.A.",
    "provider_did": "did:web:aruba.provider.gaia-x.community:participant:2fcc9256ec1186625790e592f0427f659ed67e297001c1cf06e47ce1aa12c0a1/data.json"
  },
  {
    "designation": "AWS",
    "provider_did": "did:web:aws.provider.gaia-x.community:participant:7d1507284a5757cac6b62708a4ef00bfc5d695256489cb704f12b4b9e6255df2/data.json"
  },
  ...
]
```

* Return layers:
```
curl -X GET -H "Content-Type: application/json" https://federated-catalogue-api.abc-federation.gaia-x.community/api/layers
```

example output: 
```
["IAAS", "PAAS", "SAAS"]
```


